$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'ruby_kit'

class MyFooBefore
  include RubyKit[:before_method]

  def foo(num)
    puts "Hai from foo! #{num}"
  end

  before_method :foo, :say_hello

  private

  def say_hello(num)
    puts "I am before foo, Hai ##{num}!"
  end
end

class MyFooAll
  include RubyKit

  def foo(num)
    puts "Hai from foo! #{num}"
  end

  def bar(num)
    puts "Hai from bar! #{num}"
  end

  before_method :foo, :say_hello
  after_method :bar, :say_bye


  private

  def say_hello(num)
    puts "I am before foo, Hai ##{num}!"
  end

  def say_bye(num)
    puts "I am after bar, Hai ##{num}!"
  end
end

my_foo = MyFooBefore.new
my_foo.foo 4

my_foo_all = MyFooAll.new
my_foo_all.foo(42)
my_foo_all.bar(50)
