require 'spec_helper'

describe RubyKit do

  module TestAfter
    class MyFoo
      include RubyKit[:around_method]

      def foo(num)
        sleep(1)
        @foo_time = Time.now
        return num
      end

      around_method :foo, :say_hello, :say_bye

      private

      def say_hello(num)
        @hello_time = Time.now
        @before_message = "BEFORE #{num}"
      end

      def say_bye(num)
        sleep(1)
        @bye_time = Time.now
        @after_message = "#{num} AFTER"
      end
    end
  end

  it 'calls after method' do
    num = 42
    my_foo = TestAfter::MyFoo.new

    expect(my_foo.foo(num)).to eq(num)
    expect(my_foo.instance_variable_get('@before_message')).to eq("BEFORE #{num}")
    expect(my_foo.instance_variable_get('@hello_time')).to be < my_foo.instance_variable_get('@foo_time')
    expect(my_foo.instance_variable_get('@after_message')).to eq("#{num} AFTER")
    expect(my_foo.instance_variable_get('@bye_time')).to be > my_foo.instance_variable_get('@foo_time')
  end
end
