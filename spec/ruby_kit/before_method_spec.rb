require 'spec_helper'

describe RubyKit do

  module TestBefore
    class MyFoo
      include RubyKit[:before_method]

      def foo(num)
        return num
      end

      before_method :foo, :say_hello

      private

      def say_hello(num)
        @before_message = "BEFORE #{num}"
      end
    end
  end

  it 'calls before method' do
    num = 42
    my_foo = TestBefore::MyFoo.new

    expect(my_foo.foo(num)).to eq(num)
    expect(my_foo.instance_variable_get('@before_message')).to eq("BEFORE #{num}")
  end
end
