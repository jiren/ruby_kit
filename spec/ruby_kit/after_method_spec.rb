require 'spec_helper'

describe RubyKit do

  module TestAfter
    class MyFoo
      include RubyKit[:after_method]

      def foo(num)
        @foo_time = Time.now
        return num
      end

      after_method :foo, :say_bye

      private

      def say_bye(num)
        sleep(1)
        @bye_time = Time.now
        @after_message = "#{num} AFTER"
      end
    end
  end

  it 'calls after method' do
    num = 42
    my_foo = TestAfter::MyFoo.new

    expect(my_foo.foo(num)).to eq(num)
    expect(my_foo.instance_variable_get('@after_message')).to eq("#{num} AFTER")
    expect(my_foo.instance_variable_get('@bye_time')).to be > my_foo.instance_variable_get('@foo_time')
  end
end
