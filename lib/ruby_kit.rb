require 'ruby_kit/version'
#require 'ruby_kit/before_method'

module RubyKit

  MODULES = {
    before_method: :BeforeMethod,
    after_method: :AfterMethod,
    around_method: :AroundMethod
  }

  MODULES.each do |module_name, const_name|
    autoload const_name, "ruby_kit/#{module_name}"
  end

  RubyKitError = Class.new(StandardError)

  def self.included(base)
    modules = @__modules || MODULES.keys
    # clear include module list
    @__modules = nil

    modules.each do |helper|
      if MODULES[helper]
        base.send :include, RubyKit.const_get(MODULES[helper])
      end
    end
  end

  # To list all available modules modules
  def self.modules
    MODULES.values
  end

  #
  # Include helper modules
  #
  # - To include specific modules
  #  include RubyKit[:before_method]
  # - To include all modules
  #  include RubyKit
  #
  def self.[](*vals)
    vals.each do |name|
      unless MODULES.include?(name)
         raise RubyKitr, "#{name} is invalid helper module"
      end
    end

    @__modules = vals

    return self
  end

end
