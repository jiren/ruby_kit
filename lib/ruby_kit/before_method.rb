module RubyKit
  #
  # == Example
  #   class MyFoo
  #     include RubyKit[:before_action]
  #
  #     def foo(num)
  #       puts "Hai from foo! #{num}"
  #     end
  #
  #     before_method :foo, :say_hello
  #
  #     private
  #
  #     def say_hello(num)
  #       puts "I am before foo, Hai ##{num}!"
  #     end
  #   end
  #
  #   my_foo = MyFoo.new
  #   my_foo.foo(4)
  #   > "I am before foo, Hai #4!"
  #   > "Hai from foo! 4"
  #
  module BeforeMethod
    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods
      def before_method(method, before_action)
        original_method = "original_#{method}"

        # keep reference of original method
        alias_method original_method, method

        # define method wich call first before_action and after orignal method
        define_method(method) do |*args|
          send(before_action, *args)
          send(original_method, *args)
        end
      end
    end
  end
end
